package dgf

import (
	"bytes"
	"fmt"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/ugorji/go/codec"
	"golang.org/x/net/html"
)

var (
	//データ自体をまだ受信していなかった時
	ErrSongIdNotFound = fmt.Errorf("id not found")
)

//曲データを表す構造体。
//
//HTML には http://www9.atpages.jp/stewmusic/akadb/songlist.php?key=no%3A1234 としてアクセスした際に得られた HTML データを格納する。
type Song struct {
	index    int               //何番目か
	parent   *Dgf              //親
	HTML     []byte            //HTML データ
	Received int64             //HTMLの受信日時(POSIX Time)
	Entries  []*SongEntry      //個々の曲データ情報
	doc      *goquery.Document //goquery.Document のキャッシュ
}

//個々の曲データ情報を表す構造体。
type SongEntry struct {
	index    int        //何番目か
	parent   *Song      //親
	Title    string     //楽曲のタイトル
	HasLyric bool       //歌詞があるかどうか
	lyric    *SongLyric //このエントリに対応する歌詞データ構造体のキャッシュ
}

//曲の歌詞データを保存する構造体。
type SongLyric struct {
	parent *SongEntry //親
	Body   string     //歌詞データ
}

//データベースに保存されている楽曲データを読み込む。
//
//読み込むデータによってはまだ正しく Entries 構造体が初期化されていない場合があるため注意が必要。
func (dgf *Dgf) GetSong(n int) (song *Song, err error) {
	var songBytes []byte
	songBytes, err = dgf.db.Get([]byte(fmt.Sprintf("no%d", n)))
	if err == ErrKVSKeyNotFound {
		err = ErrSongIdNotFound
		return
	}
	if err != nil {
		return
	}
	if len(songBytes) == 0 {
		err = ErrSongIdNotFound
		return
	}

	song = &Song{}
	decoder := codec.NewDecoderBytes(songBytes, &msgpackHandle)
	err = decoder.Decode(&song)

	song.parent = dgf
	song.index = n
	for k, entry := range song.Entries {
		entry.index = k
		entry.parent = song
	}
	return
}

//データベース上でのIDの番号を返す。
func (song *Song) GetIndex() int {
	return song.index
}

//格納元の Dgf のインスタンスを返す。
func (song *Song) GetParent() *Dgf {
	return song.parent
}

//HTML から goquery.Document を作成して返す。
func (song *Song) GetDocument() (doc *goquery.Document, err error) {
	if song.doc != nil {
		return song.doc, nil
	}

	var node *html.Node
	node, err = html.Parse(bytes.NewReader(song.HTML))
	if err != nil {
		return
	}

	song.doc = goquery.NewDocumentFromNode(node)
	return song.doc, nil
}

//正しくデータが受信できているなら nil が返る。
func (song *Song) HTMLIsValid() error {
	doc, err := song.GetDocument()
	if err != nil {
		return err
	}

	return htmlIsValid(doc)
}

//データがない HTML だったら true を返す。
//正しく受信できた上でデータがない時のみ true が返る。
func (song *Song) HTMLIsNotFound() (bool, error) {
	doc, err := song.GetDocument()
	if err != nil {
		return false, err
	}

	return htmlIsNotFound(doc)
}

//HTML から song.Entries 構造体の中身を構築し直す。
func (song *Song) RegenerateSongEntriesFromHTML() error {
	isNotFound, err := song.HTMLIsNotFound()
	if err != nil {
		return err
	}
	if isNotFound {
		song.Entries = make([]*SongEntry, 0)
		return nil
	}

	entries := make([]*SongEntry, 0)
	var entry *SongEntry

	doc, _ := song.GetDocument()
	contents_outline := doc.Find("#contents_outline")

	//b タグの登場ポイントと div.lyric_contents の登場ポイントを見て
	//どの歌詞がどのタイトルに対するものなのかを判定する
	contents_outline.ChildrenFiltered("b:not(:empty), div.lyric_contents").EachWithBreak(func(index int, e *goquery.Selection) bool {
		switch {
		case e.Is("b"):
			entry = &SongEntry{
				Title:    e.Text(),
				HasLyric: false,
			}
			entries = append(entries, entry)
		case e.Is("div.lyric_contents"):
			if entry == nil {
				err = fmt.Errorf("unknown lyric")
				return false
			}
			entry.HasLyric = true
			entry.lyric = &SongLyric{
				Body: strings.Replace(strings.Replace(e.Text(), "\r\n", "\n", -1), "\r", "\n", -1),
			}
		}
		return true
	})
	if err != nil {
		return err
	}

	song.Entries = entries
	return nil
}

//Song 構造体の中身をデータベースに反映させる。
func (song *Song) UpdateSong2(kvs KVS) error {
	buf := bytes.NewBufferString("")
	encoder := codec.NewEncoder(buf, &msgpackHandle)
	err := encoder.Encode(&song)
	if err != nil {
		return err
	}
	if err = kvs.Set([]byte(fmt.Sprintf("no%d", song.index)), buf.Bytes()); err != nil {
		return err
	}
	for k, entry := range song.Entries {
		key := []byte(fmt.Sprintf("no%d-lyric%d", song.index, k))
		if entry.HasLyric {
			buf.Reset()
			encoder := codec.NewEncoder(buf, &msgpackHandle)
			err = encoder.Encode(entry.lyric)
			if err != nil {
				return err
			}
			if err = kvs.Set(key, buf.Bytes()); err != nil {
				return err
			}
		} else {
			//削除失敗は無視
			kvs.Delete(key)
		}
	}
	return err
}

//Song 構造体の中身をデータベースに反映させる。
func (song *Song) UpdateSong() error {
	dgf := song.parent

	buf := bytes.NewBufferString("")
	encoder := codec.NewEncoder(buf, &msgpackHandle)
	err := encoder.Encode(&song)
	if err != nil {
		return err
	}
	if err = dgf.db.Set([]byte(fmt.Sprintf("no%d", song.index)), buf.Bytes()); err != nil {
		return err
	}
	for k, entry := range song.Entries {
		key := []byte(fmt.Sprintf("no%d-lyric%d", song.index, k))
		if entry.HasLyric {
			buf.Reset()
			encoder := codec.NewEncoder(buf, &msgpackHandle)
			err = encoder.Encode(entry.lyric)
			if err != nil {
				return err
			}
			if err = dgf.db.Set(key, buf.Bytes()); err != nil {
				return err
			}
		} else {
			//削除失敗は無視
			dgf.db.Delete(key)
		}
	}
	return err
}

//データベース上でのIDの番号を返す。
func (entry *SongEntry) GetIndex() int {
	return entry.index
}

//格納元の Song のインスタンスを返す。
func (entry *SongEntry) GetParent() *Song {
	return entry.parent
}

//SongEntry に対応した歌詞をローカルデータベースから取得して返す。
func (entry *SongEntry) GetLyric() (lyric *SongLyric, err error) {
	if !entry.HasLyric {
		err = fmt.Errorf("This is no lyric entry")
		return
	}

	//キャッシュがあったら
	if entry.lyric != nil {
		lyric = entry.lyric
		return
	}

	var lyricBytes []byte
	lyricBytes, err = entry.parent.parent.db.Get([]byte(fmt.Sprintf("no%d-lyric%d", entry.parent.index, entry.index)))
	if err != nil {
		return
	}
	if len(lyricBytes) == 0 {
		err = fmt.Errorf("lyric not found")
		return
	}

	lyric = &SongLyric{}
	decoder := codec.NewDecoderBytes(lyricBytes, &msgpackHandle)
	err = decoder.Decode(&lyric)

	lyric.parent = entry
	entry.lyric = lyric
	return
}

//格納元の SongEntry のインスタンスを返す。
func (lyric *SongLyric) GetParent() *SongEntry {
	return lyric.parent
}

//歌詞を１行毎に区切って配列で返す。空行はなくなる
func (lyric *SongLyric) GetLines() []string {
	lines := make([]string, 0)
	for _, line := range strings.Split(lyric.Body, "\n") {
		if line != "" {
			lines = append(lines, line)
		}
	}
	return lines
}

//空行で分かれた部分で区切った文字列の配列を返す。
func (lyric *SongLyric) GetBlocks() []string {
	lines := make([]string, 0)
	buf := ""
	for _, line := range strings.Split(lyric.Body, "\n") {
		if line == "" && buf != "" {
			lines = append(lines, buf)
			buf = ""
			continue
		}
		buf += line + "\n"
	}
	if buf != "" {
		lines = append(lines, buf)
	}

	return lines
}
