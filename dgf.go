package dgf

import (
	"bytes"
	"fmt"
	"github.com/ugorji/go/codec"
)

//データベースへのアクセスに使用する key-value 型データストアのインターフェイス。
//
//Get(key []byte) でキーに登録されたデータがない場合、エラーは dgf.ErrKVSKeyNotFound を返すこと。
type KVS interface {
	Get(key []byte) ([]byte, error) //保存したデータを読み込む
	Set(key, value []byte) error    //データを保存する
	Delete(key []byte) error        //登録されているデータを削除する
	Close() error                   //データベースを閉じる
}

var (
	//KVS の Get でキーに対応したデータがない場合に返すべきエラー
	ErrKVSKeyNotFound = fmt.Errorf("key not found")
)

//データベースを管理するオブジェクト。
type Dgf struct {
	db         KVS
	localMaxId *int //local-max-id のキャッシュ
}

var (
	msgpackHandle codec.MsgpackHandle
)

//Dgf のインスタンスを作成する。
func New(db KVS) *Dgf {
	return &Dgf{
		db: db,
	}
}

//データベースを閉じる。
func (dgf *Dgf) Close() error {
	return dgf.db.Close()
}

//ローカルなデータベース上における有効な ID の最大値を調べる。
//判定方法は 100 個連続で ID の欠番を検出するかどうかで行う。
func (dgf *Dgf) findLocalMaxId() (int, error) {
	lastMatch := 0
	var err error
	var isNotFound bool
	var song *Song
	for i, noMatch := 1, 0; noMatch < 100; i++ {
		song, err = dgf.GetSong(i)
		if err == ErrSongIdNotFound {
			noMatch++
			continue
		}
		if err != nil {
			return 0, err
		}

		isNotFound, err = song.HTMLIsNotFound()
		if err == ErrInvalidHTMLStateDetected {
			noMatch++
			continue
		}
		if err != nil {
			return 0, err
		}

		if isNotFound {
			noMatch++
			continue
		}

		noMatch = 0
		lastMatch = i
	}
	return lastMatch, nil
}

//ローカルデータベース上における ID の最大値を取得する。
//
//このメソッドは DB に格納されている値を返すだけで、必ずしも実態を反映しているとは限らない。
func (dgf *Dgf) GetLocalMaxId() (int, error) {
	//キャッシュがあるならそっちから
	if dgf.localMaxId != nil {
		return *dgf.localMaxId, nil
	}

	b, err := dgf.db.Get([]byte("local-max-id"))
	if err != nil {
		return 0, err
	}

	if len(b) == 0 {
		return 0, nil
	}

	var n int
	decoder := codec.NewDecoderBytes(b, &msgpackHandle)
	err = decoder.Decode(&n)
	if err != nil {
		return 0, err
	}

	dgf.localMaxId = &n
	return n, nil
}

//ローカルデータベース上における ID の最大値を調べ、データベースを更新する。
func (dgf *Dgf) UpdateLocalMaxId() error {
	n, err := dgf.findLocalMaxId()
	if err != nil {
		return err
	}

	buf := bytes.NewBufferString("")
	encoder := codec.NewEncoder(buf, &msgpackHandle)
	err = encoder.Encode(&n)
	if err != nil {
		return err
	}
	err = dgf.db.Set([]byte("local-max-id"), buf.Bytes())
	if err != nil {
		return err
	}

	//キャッシュを準備
	dgf.localMaxId = &n
	return nil
}
