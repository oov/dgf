package dgf

import (
	"testing"
)

func TestGetMaxId(t *testing.T) {
	t.Skip("skip http access")
	return

	n, err := htmlGetMaxId()
	if err != nil {
		t.Error(err)
	}

	if n < 25790 {
		t.Errorf("invalid max id")
	}

	t.Logf("max id is %d", n)
}
