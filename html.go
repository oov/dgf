package dgf

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

var (
	//解析対象の HTML の構造が想定しているものと異なる場合
	ErrInvalidHTMLStateDetected = fmt.Errorf("Invalid html state detected")
)

//指定された記事番号の記事を赤とんぼデータベースに HTTP アクセスしてデータを取得する。
//
//このメソッドは HTML の解析までは行わない。
//その処理が必要な場合は Song.RegenerateSongEntriesFromHTML() を用いて自前で行うこと。
//無駄な HTTP アクセスを繰り返さなくて済むように、受信した時点で先に保存することを推奨する。
func (dgf *Dgf) GetSongFromRemote(n int) (*Song, error) {
	//HTTP アクセス
	resp, err := http.Get(fmt.Sprintf("http://www9.atpages.jp/stewmusic/akadb/songlist.php?key=no%%3A%d", n))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	//Song 構造体の準備
	song := &Song{
		Received: time.Now().Unix(),
		index:    n,
		parent:   dgf,
	}

	//HTML 部分の読み取り
	if song.HTML, err = ioutil.ReadAll(resp.Body); err != nil {
		return nil, err
	}

	return song, nil
}

//記事IDの最大値を取得する。
func htmlGetMaxId() (n int, err error) {
	var doc *goquery.Document
	doc, err = goquery.NewDocument("http://www9.atpages.jp/stewmusic/akadb/songlist.php")
	if err != nil {
		return
	}

	text := doc.Find("a[href^='?key=no%3A']").First().Text()
	if len(text) < 4 && text[0:3] != "No." {
		err = fmt.Errorf("cannot find song max id")
		return
	}

	_, err = fmt.Sscanf(text[3:], "%d", &n)
	return
}

//正しくデータが受信できているなら nil を返す。
func htmlIsValid(doc *goquery.Document) error {
	if strings.Index(doc.Find("title").Text(), "赤とんぼ") == -1 {
		//知らないタイトルなら多分ちゃんと受信できていない
		return ErrInvalidHTMLStateDetected
	}

	return nil
}

//データがない HTML だったら true を返す。
//
//正しく受信できた上でデータがない時のみ true が返る。
func htmlIsNotFound(doc *goquery.Document) (bool, error) {
	err := htmlIsValid(doc)
	if err != nil {
		return false, err
	}

	length := doc.Find("#contents_outline").Length()
	if length == 1 {
		//作品用の枠があった
		return false, nil
	}
	if length > 1 {
		//作品用の枠がありすぎた
		return false, fmt.Errorf("too many #contents_outline(%d)", length)
	}

	//作品用の枠がない場合は「何も見つからないお（ ＾ω＾）」という
	//メッセージが表示されているはずなので、HTML 内の本文を読み取る
	var html string
	html, err = doc.Html()
	if err != nil {
		//本文部分の取得に失敗
		return false, err
	}

	if strings.Index(html, "何も見つからないお（ ＾ω＾）") == -1 {
		//作品用の枠がないのにメッセージが取得できない場合は想定外の状況
		return false, ErrInvalidHTMLStateDetected
	}

	//作品なし
	return true, nil
}
