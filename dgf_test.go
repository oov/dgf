package dgf

import (
	"testing"
)

var songs = []Song{
	Song{
		HTML: []byte(`<!DOCTYPE html>
<html lang="ja">
<title>歌詞ありデータのテスト: 赤とんぼデータベース</title>
<body>
<div id="contents_outline">
  作品用の枠
  <b>タイトル部分データ</b> <span>作者名など</span>
  <div class="lyric_contents">
    歌詞データ<br>
    つらつらと続く<br>
    <br>
    どこまでも
  </div>
</div>
</body>
</html>`),
		Received: 1373250791,
		Entries: []*SongEntry{
			&SongEntry{
				Title:    "タイトル部分データ",
				HasLyric: true,
				lyric: &SongLyric{
					Body: `歌詞データ
つらつらと続く

どこまでも`,
				},
			},
		},
	},
	Song{
		HTML: []byte(`<!DOCTYPE html>
<html lang="ja">
<title>歌詞ありで歌詞がDBに未反映の状況テスト: 赤とんぼデータベース</title>
<body>
<div id="contents_outline">
  作品用の枠
  <b>タイトル部分データ</b> <span>作者名など</span>
  <div class="lyric_contents">
    歌詞データ<br>
    つらつらと続く
  </div>
</div>
</body>
</html>`),
		Received: 1373250791,
		Entries:  nil, //正常に更新できれば0番目と同じ内容になる
	},
	Song{
		HTML: []byte(`<!DOCTYPE html>
<html lang="ja">
<title>Server Error Test</title>
<body>
そもそもリモートのサーバが上手くデータを返せなかった時のテスト
</body>
</html>`),
		Received: 1373250794,
	},
	Song{
		HTML: []byte(`<!DOCTYPE html>
<html lang="ja">
<title>削除済みデータのテスト: 赤とんぼデータベース</title>
<body>
データが見つからない時に出力されているメッセージ
何も見つからないお（ ＾ω＾）
</body>
</html>`),
		Received: 1373250794,
	},
}

type DummyAdapter struct {
	db map[string][]byte
}

func (a *DummyAdapter) Close() error {
	return nil
}

func (a *DummyAdapter) Delete(key []byte) error {
	if _, ok := a.db[string(key)]; ok {
		delete(a.db, string(key))
	}
	return nil
}

func (a *DummyAdapter) Get(key []byte) ([]byte, error) {
	if v, ok := a.db[string(key)]; ok {
		return v, nil
	} else {
		return []byte{}, nil
	}
}

func (a *DummyAdapter) Set(key, value []byte) error {
	buf := make([]byte, len(value))
	copy(buf, value)
	a.db[string(key)] = buf
	return nil
}

func putTestData(dgf *Dgf) error {
	var err error
	for k, song := range songs {
		song.index = k + 1
		song.parent = dgf
		if err = song.UpdateSong(); err != nil {
			return err
		}
	}
	return nil
}

func TestOverall(t *testing.T) {
	dgf := New(&DummyAdapter{
		db: map[string][]byte{},
	})

	err := putTestData(dgf)
	if err != nil {
		t.Error(err)
		return
	}

	var n int

	//更新前は 0
	n, err = dgf.GetLocalMaxId()
	if err != nil {
		t.Error(err)
		return
	}

	err = dgf.UpdateLocalMaxId()
	if err != nil {
		t.Error(err)
		return
	}

	//更新後は 2
	n, err = dgf.GetLocalMaxId()
	if err != nil {
		t.Error(err)
		return
	}

	if n != 2 {
		t.Fail()
	}

	t.Logf("max id is %d", n)

	//GetBlocks のテスト
	song, err := dgf.GetSong(1)
	if err != nil {
		t.Error(err)
		return
	}
	lyric, err := song.Entries[0].GetLyric()
	if err != nil {
		t.Error(err)
		return
	}

	strs := lyric.GetBlocks()
	if len(strs) != 2 {
		t.Fail()
	}
	if strs[0] != "歌詞データ\nつらつらと続く\n" || strs[1] != "どこまでも\n" {
		t.Fail()
	}
}
